<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Posts extends Model {

    protected $fillable = ['slug', 'title', 'excerpt', 'content', 'published_at', 'published', 'user_name', 'user_id'];
    protected $guarded = array(); // для токена

    // показать все статьи
    public function getPublishedPosts()
    {
        $posts = $this->latest('published_at')->published()->get();
        return $posts;
    }
    public function scopePublished($query)
    {
        $query->where('published', '=', 1);
    }

    // показать выбранную статью
    public function getPostBySlug($slug)
    {
        $posts = Posts::where('slug', '=', $slug)->get();

        return $posts;
    }

    // показать мои статьи
    public function getMyPosts($id_user)
    {
        $my_posts = Posts::where('user_id', '=', $id_user)->get();
        return $my_posts;
    }

    // показать мою статью
    public function getMyPost($id_user, $slug)
    {
        $my_posts = Posts::where('slug', '=', $slug, 'and', 'user_id', '=', $id_user)->get();
        return $my_posts;
    }

    // показать статью для редактирования
    public function getEditPost($myslug)
    {
        $editpost = Posts::where('slug', '=', $myslug)->get();

        return $editpost;
    }

    public function deletePost($id) {
        $deletedPost = Posts::where('id', '=', $id)->delete();

        return $deletedPost;
    }

    // для админа

    // получить все посты
    public function getAllPosts() {
        $all_posts = Posts::all();

        return $all_posts;
    }

    // получить не опубликованные посты
    public function getUnPublishedPosts()
    {
        $posts = Posts::where('published', '=', 0)->get();
        return $posts;
    }

    // отреактировать пост
    public function getEditAnyPost($slug)
    {
        $editpost = Posts::where('slug', '=', $slug)->get();

        return $editpost;
    }


    // получить количество постов каждого пользователя
    public function getPostsUser($all_users) {

        $posts_user = [];
        foreach ($all_users as $user) {
            $posts_user[$user->id] = User::find($user->id)->post()->get()->count();
        }

        return $posts_user;
    }

    // получить Имена всех модераторов

}
