<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Categories extends Model {

  public function getCategoryPosts() {
    $cat_posts = Posts::where('category_id', '=', 1)->get();
    return $cat_posts;
  }

}
