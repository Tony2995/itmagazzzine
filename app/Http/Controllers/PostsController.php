<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Posts;
use App\Categories;

class PostsController extends Controller {


	// получить все посты на главной странице
	public function index(Posts $postsModel)
	{

		$posts = $postsModel->getPublishedPosts();
		return view('posts.index', ['posts' => $posts]);
	}

	// показать выбранную статью
	public function show(User $user,Posts $postsModel, $slug)
	{
		$posts = $postsModel->getPostBySlug($slug);

		return view('posts.reed', ['posts' => $posts]);
	}

	//показать посты категории front-end
	public function showPostsFrontEnd(Categories $categories) {

		$category_posts = $categories->getCategoryPosts();
		return view('categories.frontend', ['posts' => $category_posts]);
	}
	// показать один пост
	public function showPostFrontEnd(Posts $postsModel, Categories $categories, $slug) {
		$posts = $postsModel->getPostBySlug($slug);
		return view('posts.reed', ['posts' => $posts]);
	}

}
