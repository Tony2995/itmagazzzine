<?php namespace App\Http\Controllers;
use App\MyPosts;
use App\User;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Posts;
use App\Admin;
use App\moderator_to_category;
use Illuminate\Http\Request;

class AdminController extends Controller
{

	public function getAllPosts(Posts $postModel)
	{
		$all_posts = $postModel->getAllPosts();

		return view('posts.index', ['posts' => $all_posts]);
	}

	public function getUnpublishedPosts(Posts $postModel)
	{

		if (Auth::guest()) {
			$status = 0;
		} else {
			$status = Auth::user()->status;
		}

		if ($status === 1) {
			$posts = $postModel->getUnPublishedPosts();
			return view('posts.index', ['posts' => $posts, 'status' => $status]);
		} else {

			return view('errors.404');
		}
	}

	public function editPost(Posts $postModel, Request $request)
	{
		// определим по slug, какой пост мы хотим изменить
		$slug =  $request->path();
		$newslug = explode("/", $slug);
		$slug = $newslug[1];
		$editpost = $postModel->getEditAnyPost($slug);

		return view('posts.editPost', ['posts' => $editpost]);
	}


	public function showUsers(Posts $modelPosts)
	{

		$all_users = User::all();
		$user_posts = $modelPosts->getPostsUser($all_users);

		return view('admin.users', ['users' => $all_users, 'user_posts' => $user_posts]);
	}

	// moderators

	public function showModerators(moderator_to_category $mod) {
		$all_users = User::all();
		$mod_names = $mod->getModeratorName($all_users);
		dd($mod_names);
		return view('admin.moderators', ['moderators' => $mod_names]);
	}




}
