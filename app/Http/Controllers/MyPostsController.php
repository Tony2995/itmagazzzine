<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use App\Posts;
use Illuminate\Http\Request;
use Illuminate\Routing\Route;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Validation\Validator;
use App\Categories;

class MyPostsController extends Controller {


	// показать все мои посты
	public function showPosts(Posts $modelPosts)
	{
		if (Auth::guest()) {
			return view('errors.404');
		}

		$id_user = Auth::user()->id;

		$my_posts = $modelPosts->getMyPosts($id_user);
		$author_name = Auth::user()->name;

		return view('posts.myPosts', ['posts' => $my_posts]);
	}

	// показать мою статью (если есть)
	public function showMyPost(Posts $modelPosts, $slug) {

		if (Auth::guest()) {
			return view('errors.404');
		}
		else
		{
			$id_user = Auth::user()->id;

			$my_posts = $modelPosts->getMyPost($id_user, $slug);

			if(sizeof($my_posts) === 0) {
				return view('errors.404');
			}
			return view('posts.reed', ['posts' => $my_posts]);
		}

	}
	// переход на страницу, создать пост
	public function create(Categories $categories)
	{
		if (Auth::user()) {
			$all_categories = $categories->all();
			//dd($all_categories);
			return view('posts.createPost', ['categories' => $all_categories]);
		}
		else {
			return view('errors.404');
		}


	}

	// запись новой статьи в БД
	public function createPost(Posts $postsModel, Request $request)
	{

		// валидация
		$valid = new ValidationCreate($request);

		$is_published = $request->get('published');

		if ($is_published === "1") {
			$all_post = $postsModel->create($request->all());

		}

		return redirect('/');
	}

	// редактируем пост
	public function editPost(Posts $modelPosts ,Request $request)
	{
		// определим по slug, какой пост мы хотим изменить
		$slug =  $request->path();
		$newslug = explode("/", $slug);
		$myslug = $newslug[2];

		$editpost = $modelPosts->getEditPost($myslug);

		return view('posts.editPost', ['posts' => $editpost]);
	}

	// сохранение поста
	public function savePost(Posts $postsModel, Request $request)
	{

		$valid = new ValidationEdit($request);


		// получаем все параметры

		$id = $request->get('id');

		$slug = $request->get('slug');
		$title = $request->get('title');
		$excerpt = $request->get('excerpt');
		$content = $request->get('content');
		$published = $request->get('published');
		$published_at = $request->get('published_at');

		// перезаписываем поля
		$my_post = $postsModel->find($id);
		$my_post->slug = $slug;
		$my_post->title = $title;
		$my_post->excerpt = $excerpt;
		$my_post->content= $content;
		$my_post->published = $published;
		$my_post->published_at = $published_at;

		// сохраняем
		$my_post->save();

		return redirect('myposts');
	}

	public function destroyPost(Posts $modelPosts, Request $request)
	{
		$id = $request->get('id');

		$deletedPost = $modelPosts->deletePost($id);

		return redirect('myposts');
	}

}

class ValidationEdit extends Controller{

	public function __construct(Request $request)
	{
		$rules = array(
			'slug' => 'required',
			'title' => 'required',
			'excerpt' => 'required|max:300',
			'content' => 'required|max:1500',
			'published_at' => 'required'
		);

		$validation = $this->validate($request, $rules);
	}

}

class ValidationCreate extends Controller{

	public function __construct(Request $request)
	{
		$rules = array(
			'slug' => 'required|unique:posts|max:10',
			'title' => 'required|unique:posts|max:15',
			'excerpt' => 'required|max:100',
			'content' => 'required|max:1500',
			'published_at' => 'required|date'
		);

		$validation = $this->validate($request, $rules);
	}

}
