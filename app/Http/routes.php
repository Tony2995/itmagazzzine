<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'PostsController@index');
Route::get('createpost', 'MyPostsController@create');
Route::get('myposts', 'MyPostsController@showPosts');
Route::post('createpost', 'MyPostsController@createPost');
Route::post('savepost', 'MyPostsController@savePost');
Route::post('destroy','MyPostsController@destroyPost');
Route::get('myposts/editpost/{slug}', 'MyPostsController@editPost');
// Route по категориям
Route::get('category/front-end', 'PostsController@showPostsFrontEnd');
Route::get('category/front-end/{slug}', 'PostsController@showPostFrontEnd');

// moderators`
Route::get('moderators', 'AdminController@showModerators');

// ADMIN ROUTE
Route::get('allposts', 'AdminController@getAllPosts');
Route::get('unpublished', 'AdminController@getUnpublishedPosts');
Route::get('editpost/{slug}', 'AdminController@editPost');
Route::get('users', 'AdminController@showUsers');


//Route::get('/post/create'); -> страница создания (view)
//Route::post('/post/create'); -> создание (data)
//Route::get('/post/{slug}'); -> читаем (view)
//Route::delete('/post/{slug}/delete'); -> удаляем (data)
//Route::get('/post/{slug}/edit'); -> страница редактирования (view)
//Route::put('/post/{slug}/edit'); -> изменения (data)

get('{slug}', ['as' => 'reed.post', 'uses' => 'PostsController@show']);
get('myposts/{slug}', ['as' => 'reed.post', 'uses' => 'MyPostsController@showMyPost']);


//Route::get('home', 'HomeController@index');

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);
