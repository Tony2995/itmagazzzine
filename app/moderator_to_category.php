<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;

class moderator_to_category extends Model {

	public function getModerator($id, $query) {

		$moderator = moderator_to_category::where('id', '=', $id)->get();
		return $moderator;
	}


	public function getModeratorName($all_users) {
		$mod_names = [];
		foreach ($all_users as $user) {
			$mod_names = User::find($user->id)->usersMod()->get();
		}
	
		return $mod_names;
	}
}
