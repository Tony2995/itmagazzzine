@extends('app')
  @section('content')
      <div class="page-header text-center">
          <h1>Front-End</h1>
      </div>
      @foreach($posts as $post)
          <div class="col-md-10 col-md-offset-1">
              <div class="panel panel-default">
                  <div class="panel-heading"><h1>{{ $post->title }}</h1></div>
                  <div class="panel-body">
                      <p>{{ $post->excerpt }}</p>
                  </div>
                  <div class="panel-footer">
                      @if(Auth::guest() or Auth::user())
                          <div class="row">
                              <div class="col-md-3"><a class="btn btn-primary btn-lg" href="{{ 'front-end/'.$post->slug }}" role="button">Читать статью</a></div>
                              <div class="col-md-2 pull-right">Опубликованно <br> {{ $post->published_at }}</div>
                              <div class="col-md-1 pull-right">Автор <br>{{ $post->user_name }}</div>
                          </div>
                      @endif
                  </div>
              </div>
          </div>
      @endforeach
  @stop
