@extends('app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2 text-center">
                <h1>Редактировать статью</h1>
                @if(count($errors) > 0)
                    <div class="row">
                        <div class="col-md-12">
                            <ul>
                                @foreach($errors->all() as $error)
                                    <div class="alert alert-danger">
                                        <strong>{{$error}}</strong>
                                    </div>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                @endif
                @foreach($posts as $post)
                    {!! Form::open(array('url' => '/savepost')) !!}
                        {!! Form::token() !!}

                        <div class="form-group">
                            {!! Form::label('url статьи') !!}
                            {!! Form::text( 'slug', $post->slug, ['class' => 'form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('Заголовок') !!}
                            {!! Form::text( 'title', $post->title, ['class' => 'form-control'] ) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('Текст презентации') !!}
                            {!! Form::textarea( 'excerpt', $post->excerpt, ['class' => 'form-control'] ) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('Текст статьи') !!}
                            {!! Form::textarea( 'content', $post->content, ['class' => 'form-control'] ) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('Опубликовать') !!}
                            {!! Form::checkbox( 'published', 1, $post->published) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('Опубликовать: ') !!}
                            {!! Form::input( 'date', 'published_at' , $post->published_at, ['class' => 'form-control']) !!}

                        </div>
                        <input type="hidden" name="id" value="{{ $post->id }}">
                        <input type="hidden" name="author_id" value="{{ $post->author_id }}">

                        <div class="form-group">
                            {!! Form::submit( 'Сохранить', ['class' => 'btn btn-primary']) !!}
                        </div>

                    {!! Form::close() !!}
                @endforeach
            </div>
        </div>
    </div>
@stop