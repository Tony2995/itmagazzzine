@extends('app')
@section('content')
    <div class="container">
        @foreach($posts as $post)
            <div class="row">
                <div class="col-md-12">
                    <h2>{{ $post->title }}</h2>
                </div>
            </div>
            <p>{!! $post->content !!}</p>

            <div class="page-header"></div>
        @endforeach
    </div>
@stop

