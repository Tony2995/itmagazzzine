@extends('app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1 class="page-header text-center">IT Magazine</h1>
            </div>
        </div>
    </div>
    @if(Auth::user() and Auth::user()->status === 1)
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="btn-group btn-group-justified page-header" role="group" aria-label="...">
                    <div class="btn-group" role="group">
                        <a href="/" class="btn btn-default">Опубликованные статьи</a>
                    </div>

                    <div class="btn-group" role="group">
                        <a href="unpublished" class="btn btn-default">Не опубликованные статьи</a>
                    </div>
                    <div class="btn-group" role="group">
                        <a href="allposts" class="btn btn-default">Все статьи</a>
                    </div>

                    <div class="btn-group" role="group">
                        <a href="createpost" class="btn btn-default">Создать статью</a>
                    </div>
                    <div class="btn-group" role="group">
                        <a href="users" class="btn btn-default">Пользователи</a>
                    </div>
                    <div class="btn-group" role="group">
                        <a href="moderators" class="btn btn-default">Модераторы</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endif
    <div class="container">
      <div class="row">
        <div class="col-md-12">
            <div class="btn-group btn-group-justified page-header" role="group" aria-label="...">
                <div class="btn-group" role="group">
                    <a href="category/front-end" class="btn btn-default">Front-end</a>
                </div>
                <div class="btn-group" role="group">
                    <a href="category/back-end" class="btn btn-default">Back-end</a>
                </div>
            </div>
        </div>
      </div>
    </div>



    <div class="container">
        <div class="row">
            @foreach($posts as $post)
                <div class="col-md-10 col-md-offset-1">
                    <div class="panel panel-default">
                        <div class="panel-heading"><h1>{{ $post->title }}</h1></div>
                        <div class="panel-body">
                            <p>{{ $post->excerpt }}</p>
                        </div>
                        <div class="panel-footer">
                            @if(Auth::user() and Auth::user()->status === 1)
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="btn-group btn-group-justified" role="group" aria-label="...">
                                            <div class="btn-group" role="group">
                                                <a class="btn btn-info" href="{{ $post->slug }}" role="button">Читать статью</a>
                                            </div>
                                            <div class="btn-group" role="group">
                                                <a class="btn btn-primary" href="{{ 'editpost/'.$post->slug }}" role="button">Редактировать</a>
                                            </div>
                                            <div class="btn-group" role="group">
                                                {!! Form::open(array('url' => '/destroy')) !!}
                                                <input type="hidden" name="_method" value="POST">
                                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                <input type="hidden" name="id" value="{{ $post->id }}">
                                                <button class="btn btn-danger">Удалить</button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2 pull-right">Опубликованно {{ $post->published_at }}</div>
                                    <div class="col-md-1 pull-right">Автор <br>{{ $post->user_name }}</div>
                                </div>
                            @endif


                            @if(Auth::guest() or Auth::user() and Auth::user()->status === 2)
                                <div class="row">
                                    <div class="col-md-3"><a class="btn btn-primary btn-lg" href="{{ $post->slug }}" role="button">Читать статью</a></div>
                                    <div class="col-md-2 pull-right">Опубликованно <br> {{ $post->published_at }}</div>
                                    <div class="col-md-1 pull-right">Автор <br>{{ $post->user_name }}</div>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
    @if(Auth::user() and Auth::user()->status === 2)
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="btn-group" role="group">
                    <a href="createpost" class="btn btn-default btn-lg ">Создать статью</a>
                </div>
            </div>
        </div>
    </div>
    @endif
@stop
