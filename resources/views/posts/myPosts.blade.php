@extends('app')
@section('content')
    <div class="page-header text-center">
        <h1>Мои статьи</h1>
    </div>
    @foreach($posts as $post)
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading"><h1>{{ $post->title }}</h1></div>
                <div class="panel-body">
                    <p>{{ $post->excerpt }}</p>
                </div>
                <div class="panel-footer">
                    <div class="row">
                       <div class="col-md-6">
                           <div class="btn-group btn-group-justified" role="group" aria-label="...">
                               <div class="btn-group" role="group">
                                   <a class="btn btn-info" href="{{ 'myposts/'.$post->slug }}" role="button">Читать статью</a>
                               </div>
                               <div class="btn-group" role="group">
                                   <a class="btn btn-primary" href="{{ 'myposts/editpost/'.$post->slug }}" role="button">Редактировать</a>
                               </div>
                               <div class="btn-group" role="group">
                                   {!! Form::open(array('url' => '/destroy')) !!}
                                       <input type="hidden" name="_method" value="POST">
                                       <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                       <input type="hidden" name="id" value="{{ $post->id }}">
                                       <button class="btn btn-danger">Удалить</button>
                                   </form>
                               </div>
                           </div>
                       </div>
                        <div class="col-md-2 pull-right">Опубликованно <br> {{ $post->published_at }}</div>
                    </div>
                </div>
            </div>
        </div>

    @endforeach
@stop