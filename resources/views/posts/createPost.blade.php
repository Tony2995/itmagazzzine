@extends('app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2 text-center">
                <h1>Создать статью</h1>
                @if(count($errors) > 0)
                    <div class="row">
                        <div class="col-md-12">
                            <ul>
                                @foreach($errors->all() as $error)
                                        <div class="alert alert-danger">
                                            <strong>{{$error}}</strong>
                                        </div>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                @endif

                {!! Form::open(array('url' => '/createpost')) !!}
                {!! Form::token() !!}

                <div class="form-group">
                    {!! Form::label('url статьи') !!}
                    {!! Form::text( 'slug', null, ['class' => 'form-control']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('Заголовок') !!}
                    {!! Form::text( 'title', null, ['class' => 'form-control'] ) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('Выберите категорию') !!}
                    <div class="row">
                        <div class="col-md-12">
                                <div class="btn-group btn-group-justified" role="group" aria-label="...">
                                        <select name="category_id">
                                            @foreach($categories as $category)
                                                <option value="{{ $category->id }}">{{ $category->name }}</option>
                                            @endforeach
                                        </select>
                                </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('Текст презентации') !!}
                    {!! Form::textarea( 'excerpt', null, ['class' => 'form-control'] ) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('Текст статьи') !!}
                    {!! Form::textarea( 'content', null, ['class' => 'form-control'] ) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('Опубликовать') !!}
                    {!! Form::checkbox( 'published', 1, false) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('Опубликовать: ') !!}
                    {!! Form::input( 'date', 'published_at', date('Y-m-d'), ['class' => 'form-control']) !!}
                </div>
                <input type="hidden" name="user_id" value="{{ Auth::user()->id }}">
                <input type="hidden" name="user_name" value="{{ Auth::user()->name }}">
                <input type="hidden" name="category_id" value="{{ Auth::user()->id }}">

                <div class="form-group">
                    {!! Form::submit( 'Создать', ['class' => 'btn btn-primary']) !!}
                </div>

                {!! Form::close() !!}

            </div>
        </div>
    </div>
@stop
