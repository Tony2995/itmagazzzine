@extends('app')
  @section('content')
      <div class="page-header text-center">
          <h1>Модераторы</h1>
      </div>
      <div class="row">
          <div class="col-md-12">
              @foreach($moderators as $mod)
                <?php
                $id = $mod['id'];
                $name = $moderators[$id];
                ?>
                  <div class="row page-header">
                      <div class="col-md-3"><h3>{{ $name }}</h3></div>
                  </div>
              @endforeach
          </div>
      </div>
  @stop
