@extends('app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1 class="page-header text-center">Пользователи</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                @foreach($users as $user)
                    <?php
                    $id = $user['id'];
                    $posts = $user_posts[$id];
                    ?>
                    <div class="row page-header">
                        <div class="col-md-3"><h3>{{ $user->name }}</h3></div>
                        <div class="col-md-3"><h3>Количество статей: {{ $posts }}</h3></div>
                    </div>
                @endforeach

            </div>
        </div>
    </div>
@stop
