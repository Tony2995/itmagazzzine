<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('posts', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('user_id')->nullable();
			$table->integer('category_id')->nullable();
			$table->string('user_name')->nullable();
			$table->string('title')->nullable();
			$table->string('slug')->unique();
			$table->text('excerpt')->nullable();
			$table->text('content')->nullable();
			$table->date('published_at')->nullable();
			$table->boolean('published')->default(false);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('posts');
	}

}
