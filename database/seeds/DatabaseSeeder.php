<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Posts;
use App\Categories;

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();


		$this->call('PostSeeder');
		$this->call('CategoriesSeeder');

	}

}
class PostSeeder extends Seeder
{
	public function run()
	{
	DB::table('Posts')->delete();
//
//		Posts::create([
//			'title' => 'Используем BEM',
//			'slug' => 'ispolzuem-bem',
//			'excerpt' => 'БЭМ (Блок-Элемент-Модификатор) — методология web-разработки, а также набор интерфейсных библиотек, фреймворков и вспомогательных инструментов.',
//			'content' => '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Assumenda dolor eaque eius eum ex illo iste maiores non obcaecati officiis pariatur perferendis reiciendis repellat tenetur, ut. Distinctio excepturi nobis odit quibusdam tenetur? Architecto aut cupiditate doloremque dolorum eveniet ipsa labore libero natus neque perspiciatis porro, repellat sapiente sequi tempore ullam?</p>',
//			'published' => true,
//			'published_at' => DB::raw('CURRENT_TIMESTAMP'),
//			'user_id' => 1,
//			'user_name' => 'admin',
//			'category_id' => 1
//
//		]);
//
//		Posts::create([
//			'title' => 'Что такое DOM',
//			'slug' => 'chto-takoe-dom',
//			'excerpt' => 'DOM (от англ. Document Object Model — «объектная модель документа») — это не зависящий от платформы и языка программный интерфейс, позволяющий программам и скриптам получить доступ к содержимому HTML, XHTML и XML-документов, а также изменять содержимое, структуру и оформление таких документов.',
//			'content' => '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Assumenda dolor eaque eius eum ex illo iste maiores non obcaecati officiis pariatur perferendis reiciendis repellat tenetur, ut. Distinctio excepturi nobis odit quibusdam tenetur? Architecto aut cupiditate doloremque dolorum eveniet ipsa labore libero natus neque perspiciatis porro, repellat sapiente sequi tempore ullam?</p>',
//			'published' => false,
//			'published_at' => DB::raw('CURRENT_TIMESTAMP'),
//			'user_id' => 1,
//			'user_name' => 'admin',
//			'category_id' => 1
//
//		]);
//
//		Posts::create([
//			'title' => 'React.js',
//			'slug' => 'react-js',
//			'excerpt' => '<p>React.js — фреймворк для создания интерфейсов от Facebook. В последнее время онлайн и на MoscowJS было много разговоров о нём. Как всегда с опозданием, я смог опробовать React только на днях. В этой статье я в двух словах расскажу о концепциях, которые стоят за фреймворком, и покажу, как быстро начать с ним работать.</p>',
//			'content' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Assumenda dolor eaque eius eum ex illo iste maiores non obcaecati officiis pariatur perferendis reiciendis repellat tenetur, ut. Distinctio excepturi nobis odit quibusdam tenetur? Architecto aut cupiditate doloremque dolorum eveniet ipsa labore libero natus neque perspiciatis porro, repellat sapiente sequi tempore ullam?',
//			'published' => false,
//			'published_at' => DB::raw('CURRENT_TIMESTAMP'),
//			'user_id' => 1,
//			'user_name' => 'admin',
//			'category_id' => 1
//		]);
	}
}

class CategoriesSeeder extends Seeder {
	public function run() {
		DB::table('Categories')->delete();

		Categories::create([
			'name' => 'Front-end',
			'slug' => 'frontend'
		]);
		Categories::create([
			'name' => 'Back-end',
			'slug' => 'backend'
		]);
	}
}




